# -*- coding: utf-8 -*
import os

# [Content] XML Footer Text
import time

def test_has_home_page_node():
    find_text_in_layout('首頁')

# [Behavior] Tap the coordinate on the screen
def test_tap_sidebar():
    tap(100, 100)
    time.sleep(2)

# 1. [Content] Side Bar Text
def test_has_side_bar_texts():
    find_text_in_layout('查看商品分類', '查訂單/退訂退款', '追蹤/買過/看過清單',
                        '智慧標籤', '其他', 'PChome 旅遊', '線上手機回收', '給24h購物APP評分')

# 2. [Screenshot] Side Bar Text
def test_side_bar_text_screenshot():
    screen_shot('Side-Bar-Text.png')

# 3. [Content] Categories
def test_categories():
    tap(945, 96)  # tap the right most side place to close the side ba
    os.system('adb shell input touchscreen swipe 551 1543 551 381 200')
    time.sleep(2)
    tap_to_expand_categories_panel()
    find_text_in_layout('精選', '3C', '周邊',
                        'NB', '通訊', '數位', '家電', '日用',
                        '食品', '生活', '運動戶外', '美妝', '衣鞋包錶')

# 4. [Screenshot] Categories
def test_categories_screenshot():
    screen_shot('Categories.png')

# 5. [Content] Categories page
def test_has_categories_page():
    tap(933, 1065)
    tap(331, 1705)

# 6. [Screenshot] Categories page
def test_categories_screenshot():
    time.sleep(1)  # turning to another page requires time
    screen_shot('Categories-Page.png')

# 7. [Behavior] Search item “switch”
def test_search_item_switch():
    tap(535, 141)  # tap the search bar
    os.system('adb shell input text "{}"'.format('switch'))
    os.system('adb shell input keyevent "KEYCODE_ENTER"')
    time.sleep(5)  # searching requires time

# 8. [Behavior] Follow an item and it should be add to the list
def test_follow_an_item_and_it_should_be_added_to_the_list():
    tap(613, 573)
    time.sleep(3)  # loading requires time
    favorite_the_current_viewing_item()
    os.system('adb shell input keyevent "KEYCODE_BACK"')
    time.sleep(1)
    os.system('adb shell input keyevent "KEYCODE_BACK"')
    time.sleep(1)
    os.system('adb shell input keyevent "KEYCODE_BACK"')
    time.sleep(1)
    tap(100, 100)
    time.sleep(2)
    tap(353,865)
    find_text_in_layout('Nintendo Switch')
    pass

# 9. [Behavior] Navigate tto the detail of item
def test_navigate_to_details_of_item():
    tap(283, 791)
    time.sleep(2)
    os.system('adb shell input touchscreen swipe 551 1543 551 381 200')
    time.sleep(2)
    tap(549, 133)

# 10. [Screenshot] Disconnetion Screen
def test_disconnection_screen_screenshot():
    os.system('adb shell svc data disable')
    time.sleep(0.5)
    screen_shot('Disconnection.png')

def tap_to_expand_categories_panel():
    tap(1005, 283)

def tap_the_first_search_item():
    tap(613, 573)
    time.sleep(3)  # loading requires time

def favorite_the_current_viewing_item():
    tap(109, 1701)
    time.sleep(2)

def tap(x, y):
    os.system('adb shell input tap {} {}'.format(x, y))
    time.sleep(0.8)

def screen_shot(filename):
    os.system('adb shell screencap -p /sdcard/screen.png')
    os.system('adb pull /sdcard/screen.png')
    if os.path.exists(filename):
        os.remove(filename)
    os.rename('screen.png', filename)


def find_text_in_layout(*text):
    xmlString = dump_layout_xml()
    for text in text:
        assert xmlString.find(text) != -1, "Cannot find text " + text

def dump_layout_xml():
    os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
    f = open('window_dump.xml', 'r', encoding="utf-8")
    return f.read()